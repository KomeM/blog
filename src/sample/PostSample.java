package sample;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PostSample
 */
@WebServlet("/PostSample")
public class PostSample extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PostSample() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		//リクエストパラメータをチェック
		//名前が入力されていない場合のエラー
		String errorMsg = "";
		if (name == null || name.length() == 0) {

			errorMsg += "名前が入力されていません<br>";
		}
		//性別が選択されていない場合のエラー
		if (gender == null || gender.length() == 0) {

			errorMsg += "性別が選択されていません<br>";

		} else {

			//性別が男の場合
			if (gender.equals("0")) {
				gender = "男性";
			}

			//性別が女の場合
			else if (gender.equals("1")) {
				gender = "女性";
			}
		}

		//表示するメッセージを設定
		String msg = name + "さん(" + gender + ")を登録しました。";
		if (errorMsg.length() != 0) {
			msg = errorMsg;
		}

		//HTMLを出力
		response.setContentType("text/html; charset = UTF-8");
		PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta charset = \"UTF-8\">");
		out.println("<title>ユーザー登録結果</title>");
		out.println("</head・>");
		out.println("<body>");
		out.println("<P>" + msg + "</P>");
		out.println("</body>");
		out.println("</html>");

		doGet(request, response);
	}

}
