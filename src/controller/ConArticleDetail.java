package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Article;
import model.Dao;

/**
 * Servlet implementation class ConArticleDetail
 */
@WebServlet("/ConArticleDetail")
public class ConArticleDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	static final String dbName = "blogdb";
	static final String user = "root", pass = "admin";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConArticleDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		String articleId = request.getParameter("id");
		Dao dao = new Dao(dbName, user, pass);

		Article ac = dao.getArticle(Integer.parseInt(articleId));

		request.setAttribute("article", ac);
		request.getRequestDispatcher("./articleDetail.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
