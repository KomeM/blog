package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Article;
import model.Dao;

@WebServlet("/ConDao")
public class ConDao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ConDao() {
		super();
	}

	static final String dbName = "blogdb";
	static final String user = "root", pass = "admin";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		Dao test = new Dao(dbName, user, pass);
		int ckResult = test.checkUser(request.getParameter("id"), request.getParameter("pass"));
		HttpSession session = request.getSession();
		if (ckResult <= 0) {
			session.setAttribute("err", "IDかパスワードが間違っています");
			getServletContext().getRequestDispatcher("/Top.jsp")
					.forward(request, response);
		} else {
			session.setAttribute("id", request.getParameter("id"));
			session.setAttribute("pass", request.getParameter("pass"));

			List<Article> articles = test.getArticle();
			request.setAttribute("article", articles);
			getServletContext().getRequestDispatcher("/articleShow.jsp")
					.forward(request, response);
		}

	}

}
