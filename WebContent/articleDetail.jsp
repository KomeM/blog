<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "beans.Article"%>

<%
	Article article = (Article)request.getAttribute("article");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- header start -->
	<!-- header end -->

	<!-- main start -->
	<div id="main">
	<h1><% article.getTitle(); %></h1>
	</div>
	<!-- main end -->

	<!-- footer start -->
	<!-- footer end -->
</body>
</html>