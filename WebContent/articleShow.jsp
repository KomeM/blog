<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "java.util.List"%>
<%@ page import = "beans.Article"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	String id = (String)session.getAttribute("id");
	String pass = (String)session.getAttribute("pass");
	List<Article> articles = (List<Article>)request.getAttribute("article");
%>
<h1>わくわくドキドキブログ</h1>
<h2>ID:<%=id %> Pass:<%=pass %></h2>
<table>
    <tr>
        <th>タイトル</th>
        <th>内容</th>
    </tr>
    <% for (Article tmp : articles) { %>
    <tr>

        <td><%= tmp.getTitle() %></td>
        <td><%= tmp.getContents() %></td>

    </tr>
    <% } %>
</table>

</body>
</html>