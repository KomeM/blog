package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import beans.Article;

public class Dao {
	String dbName, user, pass;

	public Dao(String dbName, String user, String pass) {
		this.dbName = dbName;
		this.user = user;
		this.pass = pass;
	}

	public Article getArticle(int articleId) {
		String[] strs = { "articleID", "title", "contents", "date", "location" };
		Article ac = new Article();

		String sql = "select articleID,contents,title from article where articleID=" + articleId;

		List<List<String>> result = selectSql(sql, strs);
		for (List<String> row : result) {
			ac.setArticleId(Integer.parseInt(row.get(0)));
			ac.setTitle(row.get(1));
			ac.setContents(row.get(2));
			ac.setDate(row.get(3));
			ac.setLocation(row.get(4));
		}
		return ac;
	}

	public List<Article> getArticle() {
		String[] strs = { "articleID", "title", "contents" };
		List<List<String>> result;
		List<Article> articles = new ArrayList<>();

		result = selectSql("select articleID,contents,title from article", strs);
		for (List<String> row : result) {
			Article ac = new Article();
			ac.setArticleId(Integer.parseInt(row.get(0)));
			ac.setTitle(row.get(1));
			ac.setContents(row.get(2));
			articles.add(ac);
		}

		return articles;
	}

	public int createUser(String mail, String pass) {
		String sql = "Insert into user (mailadress, passWord) values(?, ?)";
		String[] values = { mail, pass };

		return upSertSql(sql, values);
	}

	public int updateUser(int userId, String mail, String pass) {
		String sql = "UPDATE user SET mailAdress = ?,passWord = ? WHERE (userID = ?);";
		String[] values = { "g@g", "hhhhhhhh", String.valueOf(userId) };

		return upSertSql(sql, values);
	}

	public int createArticle(int userId, String title, String contents, String location) {
		String sql = "Insert into article (userId, title, contents, location) values(?, ?, ?, ?)";
		String[] values = { String.valueOf(userId), title, contents, location };

		return upSertSql(sql, values);
	}

	public int updateUser(int articleId, int userId, String title, String contents) {
		String sql = "UPDATE user SET userId = ?, title = ?, contents= ? WHERE (articelID = ?);";
		String[] values = { String.valueOf(userId), title, contents, String.valueOf(articleId) };

		return upSertSql(sql, values);
	}

	public int deleteUser(int userId) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		String time = sdf.format(timestamp);
		String sql = "UPDATE user SET delete_time=? WHERE (userID = ?)";

		String[] values = { time, String.valueOf(userId) };

		return upSertSql(sql, values);
	}

	public int deleteAticle(int articleId) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		String time = sdf.format(timestamp);
		String sql = "UPDATE article SET delete_time=? WHERE (articleID = ?)";

		String[] values = { time, String.valueOf(articleId) };

		return upSertSql(sql, values);
	}

	public int deleteComment(int commentId) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		String time = sdf.format(timestamp);
		String sql = "UPDATE comment SET delete_time=? WHERE (commentID = ?)";

		String[] values = { time, String.valueOf(commentId) };

		return upSertSql(sql, values);
	}

	public int deleteGood(int goodId) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		String time = sdf.format(timestamp);
		String sql = "UPDATE good SET delete_time=? WHERE (goodID = ?)";

		String[] values = { time, String.valueOf(goodId) };

		return upSertSql(sql, values);
	}

	public int upSertSql(String sql, String[] insertValue) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			try (
					Connection con = DriverManager.getConnection(
							"jdbc:mysql://localhost:3306/" + this.dbName + "?user=" + this.user + "&password="
									+ this.pass
									+ "&sslMode=DISABLED&serverTimezone=JST");
					PreparedStatement st = con.prepareStatement(sql);// ステートメントオブジェクトを生成
			) {

				for (int i = 0; i < insertValue.length; ++i) {
					//System.out.println(i);
					st.setString(i + 1, insertValue[i]);
				}
				return st.executeUpdate();// クエリーを実行して結果セットを取得

			} catch (SQLException e) {
				e.printStackTrace();
			}

		} catch (ClassNotFoundException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}

		return -1;
	}

	public List<List<String>> selectSql(String sql, String[] needString) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			try (
					Connection con = DriverManager.getConnection(
							"jdbc:mysql://localhost:3306/" + this.dbName + "?user=" + this.user + "&password="
									+ this.pass
									+ "&sslMode=DISABLED&serverTimezone=JST");
					PreparedStatement ps = con.prepareStatement(sql);// ステートメントオブジェクトを生成
			) {

				ResultSet rs = ps.executeQuery();// クエリーを実行して結果セットを取得
				// 検索された行数分ループ

				List<List<String>> result = new ArrayList<>();
				while (rs.next()) {
					List<String> rowResult = new ArrayList<>();
					for (String str : needString) {
						rowResult.add(rs.getString(str));
					}
					result.add(rowResult);
				}

				return result;

			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}

		return null;

	}

	public int checkUser(String id, String pass) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			try (
					Connection con = DriverManager.getConnection(
							"jdbc:mysql://localhost:3306/" + this.dbName + "?user=" + this.user + "&password="
									+ this.pass
									+ "&sslMode=DISABLED&serverTimezone=JST");
					PreparedStatement ps = con.prepareStatement(
							"select userID from user where mailAdress = ? and passWord = ?");// ステートメントオブジェクトを生成
			) {
				ps.setString(1, id);
				ps.setString(2, pass);
				ResultSet rs = ps.executeQuery();// クエリーを実行して結果セットを取得
				// 検索された行数分

				List<List<String>> result = new ArrayList<>();
				rs.next();
				return rs.getInt("userID");

			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}

		return -1;

	}

}
